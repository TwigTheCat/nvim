vim.g.mapleader = " "
vim.g.maplocalleader = " "

require('config.lazy')
require('config.options')

vim.api.nvim_create_augroup('AutoFormatting', {})
vim.api.nvim_create_autocmd('BufWritePre', {
	-- pattern = '*.lua',
	group = 'AutoFormatting',
	callback = function()
		vim.lsp.buf.format({ async = false })
	end,
})

local keymap = vim.keymap.set

local opts = {}

-- comment stuff
keymap("n", "<C-c>", "<cmd>lua require('Comment.api').toggle.linewise.current()<CR>", opts)
keymap("x", "<C-c>", ":lua require('Comment.api').toggle.linewise(vim.fn.visualmode())<CR>", opts)

-- moving between buffers
keymap("n", "<leader>re", ":bn<CR>", opts)
keymap("n", "<leader>er", ":bp<CR>", opts)

-- moving cursor
keymap("n", "<C-Down>", "}", opts)
keymap("n", "<C-Up>", "{", opts)
keymap("i", "<C-BS>", "<c-w>", opts)

-- moving text
keymap("n", "<A-Down>", ":m +1<CR>", opts)
keymap("n", "<A-Up>", ":m -2<CR>", opts)

-- Better paste
keymap("n", "<C-s-v>", "p", opts)
keymap("i", "<C-s-v>", "<ESC>:normal p<CR>a", opts)

-- saving
keymap("n", "<C-s>", ":w!<CR>", opts)

-- closing active buffer
keymap("n", "<C-S-w>", ":bd<CR>", opts)

-- moving to the next and previous word, seperated by whitespace
keymap("n", "<C-Right>", "E", opts)
keymap("n", "<C-Left>", "B", opts)

-- moving to the beginning or end of the current word
keymap("n", "<S-Right>", "e", opts)
keymap("n", "<S-Left>", "b", opts)

-- File Tree
keymap("n", "<C-e>", ":NvimTreeToggle<CR>", opts)
-- file browser
keymap("n", "<S-f>", ":Telescope file_browser path=%:p:h select_buffer=true<CR>", opts)
keymap("n", "<leader>ff",
	function() require("telescope.builtin").find_files({ cwd = require("telescope.utils").buffer_dir() }) end, opts)

-- Visual --

-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- moving around visual blocks
keymap("v", "<A-Down>", ":m '>+1<CR>gv=gv", opts)
keymap("v", "<A-Up>", ":m .-2<CR>gv=gv", opts)
