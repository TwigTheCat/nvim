local M = {
    "nvim-telescope/telescope-file-browser.nvim",
    dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" },
}

function M.config()
    require("telescope").setup {
        extensions = {
            file_browser = {
                hijack_netrw = true,
                theme = "ivy",
                git_status = false,
                mappings = {
                    ["i"] = {
                        -- your custom insert mode mappings
                    },
                    ["n"] = {
                        -- your custom normal mode mappings
                        ["%"] = require("telescope").extensions.file_browser.actions.create,
                        ["D"] = require("telescope").extensions.file_browser.actions.remove,
                        ["R"] = require("telescope").extensions.file_browser.actions.rename,
                    },
                },
            },
        },
    }
    require("telescope").load_extension "file_browser"
end

return M
