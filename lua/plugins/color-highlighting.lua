local M = {
    "brenoprata10/nvim-highlight-colors",
    lazy = false,
    priority = 1000,
}

function M.config()
    require('nvim-highlight-colors').setup{}
end

return M
