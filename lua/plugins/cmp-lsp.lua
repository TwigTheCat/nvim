local M = {
	"hrsh7th/cmp-nvim-lsp",
	dependencies = {
		"neovim/nvim-lspconfig"
	}
}

function M.config()
	require 'cmp'.setup {
		sources = {
			{ name = 'nvim_lsp' }
		}
	}

	-- The nvim-cmp almost supports LSP's capabilities so You should advertise it to LSP servers..
	local capabilities = require('cmp_nvim_lsp').default_capabilities()

	-- An example for configuring `clangd` LSP to use nvim-cmp as a completion engine
	require('lspconfig').gopls.setup {
		capabilities = capabilities,
		-- ...  other lspconfig configs
	}

	require('lspconfig').lua_ls.setup {
		capabilities = capabilities,
		-- ...  other lspconfig configs
	}

	require('lspconfig').rust_analyzer.setup {
		capabilities = capabilities,
		-- ...  other lspconfig configs
	}

	require('lspconfig').zls.setup {
		capabilities = capabilities,
		-- ...  other lspconfig configs
	}

	require('lspconfig.configs').v_analyzer = {
		default_config = {
			cmd = { "/home/ly/.config/v-analyzer/bin/v-analyzer", "--stdio" },
			filetypes = { "v", "vsh", "vv" },
			root_dir = require('lspconfig').util.root_pattern("v.mod", ".git"),
			settings = {}
		}
	}
	require('lspconfig').v_analyzer.setup {
		capabilities = capabilities,
		-- ...  other lspconfig configs
	}
end

return M
