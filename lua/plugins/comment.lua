local M = {
    'numToStr/Comment.nvim',
    opts = {
        -- add any options here
    }
}


function M.config()
	require('Comment').setup()
end

return M

