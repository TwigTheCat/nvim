local M = {
    "karb94/neoscroll.nvim",
    lazy = false
}

function M.config()
    require('neoscroll').setup()
end

return M
