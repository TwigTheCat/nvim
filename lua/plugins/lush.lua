local M = {
	"rktjmp/lush.nvim",
	lazy = false,
	priority = 1,
}

function M.config()
	local lush = require('lush')
	lush(require('fairy-forest.fairy-forest'))

	require('lualine').setup { options = { theme = require('fairy-forest.lualine') } }
	-- require('fairy-forest.rainbow-delimiters')
end

return M
