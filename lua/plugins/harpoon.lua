local M ={
	"ThePrimeagen/harpoon",
	branch = "harpoon2",
	lazy = false,
	name = "harpoon"
}


function M.config()
	local harpoon = require("harpoon")
	harpoon:setup()

	vim.keymap.set("n", "<S-b>", function() harpoon:list():add() end)
	vim.keymap.set("n", "<leader>b", function() harpoon.ui:toggle_quick_menu(harpoon:list()) end)
end

return M
