local lush = require('lush')
-- local hsl = lush.hsl
local colors = require('fairy-forest.colors')

-- LSP/Linters mistakenly show `undefined global` errors in the spec, they may
-- support an annotation like the following. Consult your server documentation.
---@diagnostic disable: undefined-global
local theme = lush(function(injected_functions)
  local sym = injected_functions.sym
  return {
    -- The following are the Neovim (as of 0.8.0-dev+100-g371dfb174) highlight
    -- groups, mostly used for styling UI elements.
    -- Comment them out and add your own properties to override the defaults.
    -- An empty definition `{}` will clear all styling, leaving elements looking
    -- like the 'Normal' group.
    -- To be able to link to a group, it must already be defined, so you may have
    -- to reorder items as you go.
    --
    -- See :h highlight-groups
	--
    ColorColumn    { bg = colors.gray02, fg = colors.gray02 }, -- Columns set with 'colorcolumn'
    -- Conceal        { }, -- Placeholder characters substituted for concealed text (see 'conceallevel')
    Cursor         { fg = colors.white, bg = colors.white, gui = "bold" }, -- Character under the cursor
    -- CurSearch      { }, -- Highlighting a search pattern under the cursor (see 'hlsearch')
    -- lCursor        { }, -- Character under the cursor when |language-mapping| is used (see 'guicursor')
    -- CursorIM       { }, -- Like Cursor, but used when in IME mode |CursorIM|
    -- CursorColumn   { }, -- Screen-column at the cursor, when 'cursorcolumn' is set.
    CursorLine     { bg = colors.gray02 }, -- Screen-line at the cursor, when 'cursorline' is set. Low-priority if foreground (ctermfg OR guifg) is not set.
    Directory      { fg = colors.fg }, -- Directory names (and other special names in listings)
    DiffAdd        { bg = colors.green, fg = colors.bg }, -- Diff mode: Added line |diff.txt|
    DiffChange     { bg = colors.gray03, fg = colors.fg }, -- Diff mode: Changed line |diff.txt|
    DiffDelete     { fg = colors.error, gui = "bold" }, -- Diff mode: Deleted line |diff.txt|
    DiffText       { bg = colors.blue, fg = colors.bg }, -- Diff mode: Changed text within a changed line |diff.txt|
    -- EndOfBuffer    { }, -- Filler lines (~) after the end of the buffer. By default, this is highlighted like |hl-NonText|.
    -- TermCursor     { }, -- Cursor in a focused terminal
    -- TermCursorNC   { }, -- Cursor in an unfocused terminal
    ErrorMsg       { bg = colors.error, fg = colors.bg }, -- Error messages on the command line
    -- VertSplit      { }, -- Column separating vertically split windows
    -- Folded         { }, -- Line used for closed folds
    -- FoldColumn     { }, -- 'foldcolumn'
    -- SignColumn     { }, -- Column where |signs| are displayed
    IncSearch      { bg = colors.gray04 }, -- 'incsearch' highlighting; also used for the text replaced with ":s///c"
    Substitute     { fg = colors.bg, bg = colors.white }, -- |:substitute| replacement text highlighting
    LineNr         { fg = colors.gray03 }, -- Line number for ":number" and ":#" commands, and when 'number' or 'relativenumber' option is set.
    -- LineNrAbove    { }, -- Line number for when the 'relativenumber' option is set, above the cursor line
    -- LineNrBelow    { }, -- Line number for when the 'relativenumber' option is set, below the cursor line
    CursorLineNr   { fg = colors.gray05 }, -- Like LineNr when 'cursorline' or 'relativenumber' is set for the cursor line.
    -- CursorLineFold { }, -- Like FoldColumn when 'cursorline' is set for the cursor line
    -- CursorLineSign { }, -- Like SignColumn when 'cursorline' is set for the cursor line
    -- MatchParen     { }, -- Character under the cursor or just before it, if it is a paired bracket, and its match. |pi_paren.txt|
    -- ModeMsg        { }, -- 'showmode' message (e.g., "-- INSERT -- ")
    -- MsgArea        { }, -- Area for messages and cmdline
    -- MsgSeparator   { }, -- Separator for scrolled messages, `msgsep` flag of 'display'
    MoreMsg        { fg = colors.cyan }, -- |more-prompt|
    -- NonText        { }, -- '@' at the end of the window, characters from 'showbreak' and other characters that do not really exist in the text (e.g., ">" displayed when a double-wide character doesn't fit at the end of the line). See also |hl-EndOfBuffer|.
    Normal         { bg = colors.bg, fg = colors.fg }, -- Normal text
    -- NormalFloat    { }, -- Normal text in floating windows.
    -- FloatBorder    { }, -- Border of floating windows.
    -- FloatTitle     { }, -- Title of floating windows.
    -- NormalNC       { }, -- normal text in non-current windows
    Pmenu          { bg = colors.background }, -- Popup menu: Normal item.
    PmenuSel       { fg = colors.gray04 }, -- Popup menu: Selected item.
    -- PmenuKind      { }, -- Popup menu: Normal item "kind"
    -- PmenuKindSel   { }, -- Popup menu: Selected item "kind"
    -- PmenuExtra     { }, -- Popup menu: Normal item "extra text"
    -- PmenuExtraSel  { }, -- Popup menu: Selected item "extra text"
    PmenuSbar      { fg = colors.fg }, -- Popup menu: Scrollbar.
    PmenuThumb     { fg = colors.fg }, -- Popup menu: Thumb of the scrollbar.
    Question       { fg = colors.cyan, gui = "italic" }, -- |hit-enter| prompt and yes/no questions
    QuickFixLine   { fg = colors.cyan, gui = "italic" }, -- Current |quickfix| item in the quickfix window. Combined with |hl-CursorLine| when the cursor is there.
    Search         { bg = colors.white, fg = colors.bg }, -- Last search pattern highlighting (see 'hlsearch'). Also used for similar items that need to stand out.
    -- SpecialKey     { }, -- Unprintable characters: text displayed differently from what it really is. But not 'listchars' whitespace. |hl-Whitespace|
    -- SpellBad       { }, -- Word that is not recognized by the spellchecker. |spell| Combined with the highlighting used otherwise.
    -- SpellCap       { }, -- Word that should start with a capital. |spell| Combined with the highlighting used otherwise.
    -- SpellLocal     { }, -- Word that is recognized by the spellchecker as one that is used in another region. |spell| Combined with the highlighting used otherwise.
    -- SpellRare      { }, -- Word that is recognized by the spellchecker as one that is hardly ever used. |spell| Combined with the highlighting used otherwise.
    StatusLine     { fg = colors.status_line_fg }, -- Status line of current window
    -- StatusLineNC   { }, -- Status lines of not-current windows. Note: If this is equal to "StatusLine" Vim will use "^^^" in the status line of the current window.
    TabLine        { fg = colors.fg, bg = colors.bg }, -- Tab pages line, not active tab page label
    TabLineFill    { fg = colors.fg, bg = colors.bg }, -- Tab pages line, where there are no labels
    TabLineSel     { fg = colors.green, bg = colors.gray02 }, -- Tab pages line, active tab page label
    Title          { fg = colors.red }, -- Titles for output from ":set all", ":autocmd" etc.
    -- Visual         { }, -- Visual mode selection
    -- VisualNOS      { }, -- Visual mode selection when vim is "Not Owning the Selection".
    WarningMsg     { bg = colors.yellow, fg = colors.bg }, -- Warning messages
    -- Whitespace     { }, -- "nbsp", "space", "tab" and "trail" in 'listchars'
    -- Winseparator   { }, -- Separator between window splits. Inherts from |hl-VertSplit| by default, which it will replace eventually.
    -- WildMenu       { }, -- Current match in 'wildmenu' completion
    -- WinBar         { }, -- Window bar of current window
    -- WinBarNC       { }, -- Window bar of not-current windows

    -- Common vim syntax groups used for all kinds of code and markup.
    -- Commented-out groups should chain up to their preferred (*) group
    -- by default.
    --
    -- See :h group-name
    --
    -- Uncomment and edit if you want more specific syntax highlighting.

    -- Comment        { }, -- Any comment

    -- Constant       { }, -- (*) Any constant
    String         { fg = colors.gray06 }, --   A string constant: "this is a string"
    Character      { fg = colors.gray06 }, --   A character constant: 'c', '\n'
    Number         { fg = colors.yellow }, --   A number constant: 234, 0xff
    Boolean        { fg = colors.red }, --   A boolean constant: TRUE, false
    Float          { fg = colors.yellow }, --   A floating point constant: 2.3e10

    Identifier     { fg = colors.fg }, -- (*) Any variable name
    Function       { fg = colors.green, gui = "italic" }, --   Function name (also: methods for classes)

    -- Statement      { }, -- (*) Any statement
    -- Conditional    { }, --   if, then, else, endif, switch, etc.
    -- Repeat         { }, --   for, do, while, etc.
    -- Label          { }, --   case, default, etc.
    -- Operator       { }, --   "sizeof", "+", "*", etc.
    Keyword        { fg = colors.magenta }, --   any other keyword
    -- Exception      { }, --   try, catch, throw

    -- PreProc        { }, -- (*) Generic Preprocessor
    -- Include        { }, --   Preprocessor #include
    -- Define         { }, --   Preprocessor #define
    -- Macro          { }, --   Same as Define
    -- PreCondit      { }, --   Preprocessor #if, #else, #endif, etc.

    -- Type           { }, -- (*) int, long, char, etc.
    -- StorageClass   { }, --   static, register, volatile, etc.
    -- Structure      { }, --   struct, union, enum, etc.
    -- Typedef        { }, --   A typedef

    Special        { fg = colors.purple, gui = "italic" }, -- (*) Any special symbol
    -- SpecialChar    { }, --   Special character in a constant
    -- Tag            { }, --   You can use CTRL-] on this
    -- Delimiter      { }, --   Character that needs attention
    -- SpecialComment { }, --   Special things inside a comment (e.g. '\n')
    -- Debug          { }, --   Debugging statements

    -- Underlined     { gui = "underline" }, -- Text that stands out, HTML links
    -- Ignore         { }, -- Left blank, hidden |hl-Ignore| (NOTE: May be invisible here in template)
    -- Error          { }, -- Any erroneous construct
    -- Todo           { }, -- Anything that needs extra attention; mostly the keywords TODO FIXME and XXX

    -- These groups are for the native LSP client and diagnostic system. Some
    -- other LSP clients may use these groups, or use their own. Consult your
    -- LSP client's documentation.

    -- See :h lsp-highlight, some groups may not be listed, submit a PR fix to lush-template!
    --
    -- LspReferenceText            { } , -- Used for highlighting "text" references
    -- LspReferenceRead            { } , -- Used for highlighting "read" references
    -- LspReferenceWrite           { } , -- Used for highlighting "write" references
    -- LspCodeLens                 { } , -- Used to color the virtual text of the codelens. See |nvim_buf_set_extmark()|.
    -- LspCodeLensSeparator        { } , -- Used to color the seperator between two or more code lens.
    -- LspSignatureActiveParameter { } , -- Used to highlight the active parameter in the signature help. See |vim.lsp.handlers.signature_help()|.

    -- See :h diagnostic-highlights, some groups may not be listed, submit a PR fix to lush-template!
    --
    -- DiagnosticError            { } , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
    -- DiagnosticWarn             { } , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
    -- DiagnosticInfo             { } , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
    -- DiagnosticHint             { } , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
    -- DiagnosticOk               { } , -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
    -- DiagnosticVirtualTextError { } , -- Used for "Error" diagnostic virtual text.
    -- DiagnosticVirtualTextWarn  { } , -- Used for "Warn" diagnostic virtual text.
    -- DiagnosticVirtualTextInfo  { } , -- Used for "Info" diagnostic virtual text.
    -- DiagnosticVirtualTextHint  { } , -- Used for "Hint" diagnostic virtual text.
    -- DiagnosticVirtualTextOk    { } , -- Used for "Ok" diagnostic virtual text.
    -- DiagnosticUnderlineError   { } , -- Used to underline "Error" diagnostics.
    -- DiagnosticUnderlineWarn    { } , -- Used to underline "Warn" diagnostics.
    -- DiagnosticUnderlineInfo    { } , -- Used to underline "Info" diagnostics.
    -- DiagnosticUnderlineHint    { } , -- Used to underline "Hint" diagnostics.
    -- DiagnosticUnderlineOk      { } , -- Used to underline "Ok" diagnostics.
    -- DiagnosticFloatingError    { } , -- Used to color "Error" diagnostic messages in diagnostics float. See |vim.diagnostic.open_float()|
    -- DiagnosticFloatingWarn     { } , -- Used to color "Warn" diagnostic messages in diagnostics float.
    -- DiagnosticFloatingInfo     { } , -- Used to color "Info" diagnostic messages in diagnostics float.
    -- DiagnosticFloatingHint     { } , -- Used to color "Hint" diagnostic messages in diagnostics float.
    -- DiagnosticFloatingOk       { } , -- Used to color "Ok" diagnostic messages in diagnostics float.
    -- DiagnosticSignError        { } , -- Used for "Error" signs in sign column.
    -- DiagnosticSignWarn         { } , -- Used for "Warn" signs in sign column.
    -- DiagnosticSignInfo         { } , -- Used for "Info" signs in sign column.
    -- DiagnosticSignHint         { } , -- Used for "Hint" signs in sign column.
    -- DiagnosticSignOk           { } , -- Used for "Ok" signs in sign column.

    -- Tree-Sitter syntax groups.
    --
    -- See :h treesitter-highlight-groups, some groups may not be listed,
    -- submit a PR fix to lush-template!
    --
    -- Tree-Sitter groups are defined with an "@" symbol, which must be
    -- specially handled to be valid lua code, we do this via the special
    -- sym function. The following are all valid ways to call the sym function,
    -- for more details see https://www.lua.org/pil/5.html
    --
    -- sym("@text.literal")
    -- sym('@text.literal')
    -- sym"@text.literal"
    -- sym'@text.literal'
    --
    -- For more information see https://github.com/rktjmp/lush.nvim/issues/109

    sym"@text.literal"      { fg = colors.gray06 }, -- Comment
    sym"@text.reference"    { fg = colors.red }, -- Identifier
    sym"@text.title"        { fg = colors.red }, -- Title
    sym"@text.uri"          { fg = colors.blue, gui = "underline" }, -- Underlined
    sym"@text.underline"    { gui = "underline" }, -- Underlined
    sym"@text.todo"         { fg = colors.red, gui = "bold" }, -- Todo
    sym"@comment"           { fg = colors.beige, gui = "italic" }, -- Comment
    sym"@punctuation"       { fg = colors.fg }, -- Delimiter
    sym"@constant"          { fg = colors.red }, -- Constant
    sym"@constant.builtin"  { fg = colors.red }, -- Special
    sym"@constant.macro"    { fg = colors.red }, -- Define
    sym"@define"            { fg = colors.red }, -- Define
    sym"@macro"             { fg = colors.green }, -- Macro
    sym"@string"            { fg = colors.gray06 }, -- String
    sym"@string.escape"     { fg = colors.gray06 }, -- SpecialChar
    sym"@string.special"    { fg = colors.gray06 }, -- SpecialChar
    sym"@character"         { fg = colors.gray06 }, -- Character
    sym"@character.special" { fg = colors.gray06 }, -- SpecialChar
    sym"@number"            { fg = colors.yellow }, -- Number
    sym"@boolean"           { fg = colors.red }, -- Boolean
    sym"@float"             { fg = colors.yellow }, -- Float
    sym"@function"          { fg = colors.green, gui = "italic" }, -- Function
    sym"@function.builtin"  { fg = colors.green }, -- Special
    sym"@function.macro"    { fg = colors.green }, -- Macro
    sym"@parameter"         { fg = colors.fg }, -- Identifier
    sym"@method"            { fg = colors.yellow }, -- Function
    sym"@field"             { fg = colors.yellow }, -- Identifier
    sym"@property"          { fg = colors.yellow }, -- Identifier
    sym"@constructor"       { fg = colors.red }, -- Special
    sym"@conditional"       { fg = colors.purple }, -- Conditional
    sym"@repeat"            { fg = colors.purple, gui = "italic" }, -- Repeat
    sym"@label"             { fg = colors.purple }, -- Label
    sym"@operator"          { fg = colors.purple }, -- Operator
    sym"@keyword"           { fg = colors.magenta }, -- Keyword
    sym"@exception"         { fg = colors.red, gui = "bold" }, -- Exception
    sym"@variable"          { fg = colors.fg }, -- Identifier
    sym"@type"              { fg = colors.cyan }, -- Type
    sym"@type.definition"   { fg = colors.green, gui = "italic" }, -- Typedef
    sym"@storageclass"      { fg = colors.cyan }, -- StorageClass
    sym"@structure"         { fg = colors.cyan }, -- Structure
    sym"@namespace"         { fg = colors.purple }, -- Identifier
    sym"@include"           { fg = colors.purple, gui = "italic" }, -- Include
    sym"@preproc"           { fg = colors.cyan }, -- PreProc
    sym"@debug"             { fg = colors.yellow, gui = "italic" }, -- Debug
    sym"@tag"               { fg = colors.red, gui = "italic" }, -- Tag
}
end)

-- Return our parsed theme for extension or use elsewhere.
return theme

-- vi:nowrap
