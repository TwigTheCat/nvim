local colors = require('fairy-forest.colors')

vim.api.nvim_command("highlight RainbowDelimiterRed guifg=" .. colors.magenta)
vim.api.nvim_command("highlight RainbowDelimiterYellow guifg=" .. colors.yellow)
vim.api.nvim_command("highlight RainbowDelimiterBlue guifg=" .. colors.blue)
vim.api.nvim_command("highlight RainbowDelimiterGreen guifg=" .. colors.green)
vim.api.nvim_command("highlight RainbowDelimiterViolet guifg=" ..colors.purple)
vim.api.nvim_command("highlight RainbowDelimiterCyan guifg=" .. colors.cyan)
