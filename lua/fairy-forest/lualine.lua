local colors = require('fairy-forest.colors')

return {
  normal = {
    a = {bg = colors.gray03, fg = colors.bg, gui = 'bold'},
    b = {bg = colors.bg, fg = colors.white},
    c = {bg = colors.bg, fg = colors.white}
  },
  insert = {
    a = {bg = colors.blue, fg = colors.bg, gui = 'bold'},
    b = {bg = colors.bg, fg = colors.white},
    c = {bg = colors.bg, fg = colors.white}
  },
  visual = {
    a = {bg = colors.yellow, fg = colors.bg, gui = 'bold'},
    b = {bg = colors.bg, fg = colors.white},
    c = {bg = colors.bg, fg = colors.white}
  },
  replace = {
    a = {bg = colors.red, fg = colors.bg, gui = 'bold'},
    b = {bg = colors.bg, fg = colors.white},
    c = {bg = colors.bg, fg = colors.white}
  },
  command = {
    a = {bg = colors.green, fg = colors.bg, gui = 'bold'},
    b = {bg = colors.bg, fg = colors.white},
    c = {bg = colors.bg, fg = colors.white}
  },
  inactive = {
    a = {bg = colors.gray03, fg = colors.gray02, gui = 'bold'},
    b = {bg = colors.bg, fg = colors.gray02},
    c = {bg = colors.bg, fg = colors.gray02}
  }
}
